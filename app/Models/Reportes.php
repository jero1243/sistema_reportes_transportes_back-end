<?php

namespace App\Models;

use App\Events\ActionCreate;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Reportes extends Model
{

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'anonimo' => 'boolean',
    ];
    protected $fillable = ['ruta','numero_eco','sitio','anonimo','descripcion','user_id','customer_id','municipio_id','type_id','inspector_id','supervisor_id'];


    public function user()
    {
        return $this->hasOne(User::Class);
    }

    public function customer()
    {
        return $this->hasOne(Customer::Class);
    }

    public function municipio()
    {
        return $this->hasOne(Municipios::Class);
    }

    public function type()
    {
        return $this->hasOne(Type::Class);
    }

    public function inspector()
    {
        return $this->hasOne(User::Class, 'id', 'inspector_id');
    }

    public function supervisor()
    {
        return $this->hasOne(User::Class, 'id', 'supervisor_id');
    }

}
