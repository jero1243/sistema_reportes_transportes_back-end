<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Bitacora extends Model
{

    public function user()
    {
        return $this->hasOne(User::Class);
    }

    public function previousUser()
    {
        return $this->hasOne(User::Class, 'id', 'previous_user_id');
    }

    public function reporte()
    {
        return $this->hasOne(Reportes::Class);
    }

}
