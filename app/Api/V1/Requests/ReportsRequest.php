<?php

namespace App\Api\V1\Requests;

use Config;
use Dingo\Api\Http\FormRequest;

class ReportsRequest extends FormRequest
{
    public function rules()
    {
        
        return [
            
            'ruta' => ['required'],
            'sitio' => ['required'],
            'anonimo' => ['required'],
            'descripcion' => ['required'],
            'user_id' => ['numeric'],
            'customer_id' => ['numeric'],
            'municipio_id' => ['numeric'],
            'type_id' => ['numeric'],
            'inspector_id' => ['numeric'],
            'supervisor_id' => ['numeric'],

        ];
    }

    public function authorize()
    {
        return $this->user()->hasPermissionTo('create_sub');
    }
}
