<?php


namespace App\Api\V1\Controllers;


use App\Api\V1\Requests\MunicipioRequest;
use App\Http\Controllers\Controller;
use App\Repositories\MunicipioRepository;
use Auth;

class MunicipioController extends Controller
{
    /**
     * @var MunicipioRepository
     */
    protected $municipioRepository;

    /**
     * UserController constructor.
     *
     * @param MunicipioRepository $municipioRepository
     */
    public function __construct(MunicipioRepository $municipioRepository)
    {
        $this->MunicipioRepository = $municipioRepository;
    }

    public function createMunicipio(MunicipioRequest $request)
    {
<<<<<<< HEAD
        $municipio = $this->MunicipioRepository->createMunicipio($request->all());
=======
        $municipio = $this->municipioRepository->createMunicipio($request->all());
>>>>>>> ea33f68f995cf9aee93af41fdc6b33da5016b321
        return response()->json([
            'status' => 'ok',
            'municipio' => $municipio,
        ], 201);
    }
    public function getMunicipios()
    {
        $municipios = $this->municipioRepository->getMunicipios();

        return $municipios;
    }

    public function updateMunicipio(MunicipioRequest $request)
    {
        $Municipio = $this->MunicipioRepository->updateMunicipio($request->all());
        return response()->json([
            'status' => 'ok',
            'municipio' => $Municipio
        ], 201);
    }
    public function deleteMunicipio(MunicipioRequest $request)
    {
        $Municipio = $this->MunicipioRepository->deleteMunicipio($request->all());
        return response()->json([
            'status' => 'ok',
<<<<<<< HEAD
            'message' => 'Eliminad correctamente'
=======
            'message' => 'Eliminado correctamente'
>>>>>>> ea33f68f995cf9aee93af41fdc6b33da5016b321
        ], 201);
    }
}
