<?php

namespace App\Api\V1\Controllers\Auth;

use App\Repositories\UserRepository;
use Config;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\Auth\SignUpRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SignUpController extends Controller
{

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * UserController constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function signUp(SignUpRequest $request, JWTAuth $JWTAuth)
    {
        $user = $this->userRepository->create($request->all());
        if (!$user->save()) {
            throw new HttpException(500);
        }
        $token= $JWTAuth->fromUser($user);
        return response()->json([
            'token'=>$token,
            'status' => 'ok'
        ], 201);
    }
}
