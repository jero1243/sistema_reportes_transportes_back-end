<?php


namespace App\Api\V1\Controllers;


use App\Api\V1\Requests\CustomersRequest;
use App\Http\Controllers\Controller;
use App\Repositories\CustomersRepository;
use Auth;

class CustomersController extends Controller
{
    /**
     * @var CustomersRepository
     */
    protected $customersRepository;

    /**
     * UserController constructor.
     *
     * @param CustomersRepository $customersRepository
     */
    public function __construct(CustomersRepository $customersRepository)
    {
        $this->CustomersRepository = $customersRepository;
    }

    public function createCustomer(CustomersRequest $request)
    {
        $customer = $this->CustomersRepository->createCustomer($request->all());
        return response()->json([
            'status' => 'ok',
            'customer' => $customer,
        ], 201);
    }
 public function getCustomers()
    {
        $customers = $this->CustomersRepository->getCustomers();

        return $customers;
    }

    public function updateCustomer(CustomersRequest $request)
    {
        $customer = $this->CustomersRepository->updateCustomer($request->all());
        return response()->json([
            'status' => 'ok',
            'customer' => $customer
        ], 201);
    }
    public function deleteCustomer(CustomersRequest $request)
    {
        $customer = $this->CustomersRepository->deleteCustomer($request->all());
        return response()->json([
            'status' => 'ok',
            'message' => 'Eliminado correctamente'
        ], 201);
    }

}
