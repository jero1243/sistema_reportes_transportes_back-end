<?php


namespace App\Api\V1\Controllers;


use App\Api\V1\Requests\ReportsRequest;
use App\Http\Controllers\Controller;
use App\Repositories\ReportsRepository;
use Auth;

class ReportsController extends Controller
{
    /**
     * @var CustomersRepository
     */
    protected $ReportsRepository;

    /**
     * UserController constructor.
     *
     * @param CustomersRepository $customersRepository
     */
    public function __construct(ReportsRepository $ReportsRepository)
    {
        $this->ReportsRepository = $ReportsRepository;
    }

    public function createReport(ReportsRequest $request)
    {
        $Report = $this->ReportsRepository->createReport($request->all());
        return response()->json([
            'status' => 'ok',
            'reports' => $Report,
        ], 201);
    }

    public function getReport(){
        $reports= $this->ReportsRepository->getReport();

        return  $reports;
    }

    public function getBitacora(){
        $bitacora = $this->ReportsRepository->getBitacora();

        return  $bitacora;
    }

    public function updateReport(ReportsRequest $request)
    {
        $report = $this->ReportsRepository->updateReport($request->all());
        return response()->json([
            'status' => 'ok',
            'report' => $report
        ], 201);
    }

}
