<?php


namespace App\Repositories;

use App\User;
use Illuminate\Support\Facades\DB;

class UserRepository
{
    /**
     * UserRepository constructor.
     *
     * @param  User  $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $data
     *
     * @throws \Exception
     * @throws \Throwable
     * @return User
     */
    public function create(array $data): User
    {
        return DB::transaction(function () use ($data) {
            $user = $this->model::create($data);

            // See if adding any additional permissions
            if (!isset($data['permissions']) || !count($data['permissions'])) {
                $data['permissions'] = [];
            }

            if ($user) {
                // User must have at least one role
                if (!array_key_exists('roles', $data) || !count($data['roles'])) {
                    $user->syncRoles([config('access.users.default_role')]);
                } else {
                    // Add selected roles/permissions
                    $user->syncRoles($data['roles']);
                    $user->syncPermissions($data['permissions']);
                }
                return $user;
            }
            return null;
        });
    }
}
