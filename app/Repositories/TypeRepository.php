<?php

namespace App\Repositories;

use Exception;
use App\Models\Type;

class TypeRepository
{

    public function createType(array $data): Type
    {
        $type = new Type($data);
        if (!$type->save()) {
            throw new Exception("Error Saving");
        }

        return $type;
    }
    public function getType()
    {
        $type = Type::all();
        return $type;
    }

    public function updateType(array $data)
    {
        $type = Type::find($data['id']);
        $type->name = $data['name'];

        if (!$type->update()) {
            throw new Exception("Error Saving");
        }

        return $type;
    }

    public function deleteType(array $data)
    {
        $type = Type::find($data['id']);
        $type->name = $data['name'];

        if (!$type->delete()) {
            throw new Exception("Error Saving");
        }

        return $type;
    }
}
