<?php


namespace App\Repositories;

use App\Events\ActionCreate;
use Exception;
use App\Models\Reportes;
<<<<<<< HEAD
use App\Models\Bitacora;
=======
use App\User;
>>>>>>> ea33f68f995cf9aee93af41fdc6b33da5016b321

class ReportsRepository
{

    public function createReport(array $data): Reportes
    {
        $users=User::all()->random(1)->first();
        

        $data['supervisor_id']=$users->id;

        $reports = new Reportes($data);
        if (!$reports->save()) {
            throw new Exception("Error Saving");
        }

        return $reports;
    }

    public function getReport(){
        $report = Reportes::all();

        return $report;
    }
    public function getBitacora(){
        $bitacora = Bitacora::all();

        return $bitacora;
    }

    public function updateReport(array $data)
    { 
        $file = $data['file'];
        $name = $file->getClientOriginalName();
        \Storage::disk('local')->put($name,  \File::get($file));
        $report = Reportes::find($data['id']);
        $report->estado = $data['estado'];
        $report->ruta = $data['ruta'];
        $report->sitio = $data['sitio'];
        $report->anonimo = $data['anonimo'];
        $report->descripcion = $data['descripcion'];

        if (!$report->update()) {
            throw new Exception("Error Saving");
        }

        return $report;
    }



    
}