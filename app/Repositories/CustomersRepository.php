<?php


namespace App\Repositories;

use Exception;
use App\Models\Customer;

class CustomersRepository
{

    public function createCustomer(array $data): Customer
    {
        $customers = new Customer($data);
        if (!$customers->save()) {
            throw new Exception("Error Saving");
        }

        return $customers;
    }
  public function getCustomers()
    {
        $customers = Customer::all();

        if (!$customers) {
            throw new Exception("Error Saving");
        }

        return $customers;
    }

    public function updateCustomer(array $data)
    {
        $customer = Customer::find($data['id']);
        $customer->name = $data['name'];

        if (!$customer->update()) {
            throw new Exception("Error Saving");
        }

        return $customer;
    }

    public function deleteCustomer(array $data)
    {
        $customer = Customer::find($data['id']);

        if (!$customer->delete()) {
            throw new Exception("Error Deleting");
        }

        return $customer;
    }

}
