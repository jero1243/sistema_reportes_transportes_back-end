<?php


namespace App\Repositories;

use Exception;
use App\Models\Municipios;

class MunicipioRepository
{

    public function createMunicipio(array $data): Municipios
    {
        $municipio = new Municipios($data);
        if (!$municipio->save()) {
            throw new Exception("Error Saving");
        }

        return $municipio;
    }

    public function getMunicipios()
    {
        $municipios = Municipios::all();

        if (!$municipios) {
            throw new Exception("Error Saving");
        }

        return $municipios;
    }

    public function updateMunicipio(array $data)
    {
        $municipio = Municipios::find($data['id']);
        $municipio->name = $data['name'];

        if (!$municipio->update()) {
            throw new Exception("Error Saving");
        }

        return $municipio;
    }

    public function deleteMunicipio(array $data)
    {
        $municipio = Municipios::find($data['id']);
<<<<<<< HEAD:app/Repositories/MunicipioRepository.php
        $municipio->name = $data['name'];

        if (!$municipio->update()) {
            throw new Exception("Error Saving");
=======

        if (!$municipio->delete()) {
            throw new Exception("Error Deleting");
>>>>>>> ea33f68f995cf9aee93af41fdc6b33da5016b321:app/Repositories/ListsRepository.php
        }

        return $municipio;
    }
}
