<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'auth'], function (Router $api) {
        $api->post('signup', 'App\\Api\\V1\\Controllers\\Auth\\SignUpController@signUp');
        $api->post('login', 'App\\Api\\V1\\Controllers\\Auth\\LoginController@login');

     $api->put('user', 'App\\Api\\V1\\Controllers\\Auth\\UserController@updateUser');


        $api->post('recovery', 'App\\Api\\V1\\Controllers\\Auth\\ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'App\\Api\\V1\\Controllers\\Auth\\ResetPasswordController@resetPassword');

        $api->post('logout', 'App\\Api\\V1\\Controllers\\Auth\\LogoutController@logout');
        $api->post('refresh', 'App\\Api\\V1\\Controllers\\Auth\\RefreshController@refresh');
        $api->get('me', 'App\\Api\\V1\\Controllers\\Auth\\UserController@me');
    });
    $api->group(['middleware' => 'jwt.auth'], function (Router $api) {
        $api->post('municipio', 'App\\Api\\V1\\Controllers\\MunicipioController@createMunicipio')->middleware('role:admin_user');
        $api->get('municipios', 'App\\Api\\V1\\Controllers\\MunicipioController@getMunicipios')->middleware('role:admin_user');
        $api->put('municipio/{id}', 'App\\Api\\V1\\Controllers\\MunicipioController@updateMunicipio')->middleware('role:admin_user');
        $api->delete('municipio/{id}', 'App\\Api\\V1\\Controllers\\MunicipioController@deleteMunicipio')->middleware('role:admin_user');

        $api->post('customer', 'App\\Api\\V1\\Controllers\\CustomersController@createCustomer')->middleware('role:admin_user|visor_user');
        $api->get('customers', 'App\\Api\\V1\\Controllers\\CustomersController@getCustomers')->middleware('role:admin_user');
        $api->put('customer/{id}', 'App\\Api\\V1\\Controllers\\CustomersController@updateCustomer')->middleware('role:admin_user');
        $api->delete('customer/{id}', 'App\\Api\\V1\\Controllers\\CustomersController@deleteCustomer')->middleware('role:admin_user');

        $api->post('type', 'App\\Api\\V1\\Controllers\\TypeController@createType')->middleware('role:admin_user');
        $api->get('types', 'App\\Api\\V1\\Controllers\\TypeController@getType')->middleware('role:admin_user');
        $api->put('type/{id}', 'App\\Api\\V1\\Controllers\\TypeController@updateType')->middleware('role:admin_user');
        $api->delete('type/{id}', 'App\\Api\\V1\\Controllers\\TypeController@deleteType')->middleware('role:admin_user');

        $api->post('report', 'App\\Api\\V1\\Controllers\\ReportsController@createReport')->middleware('role:admin_user|visor_user');
        $api->get('reports', 'App\\Api\\V1\\Controllers\\ReportsController@getReport')->middleware('role:admin_user|inspector_user|gestor_user|visor_user');
        $api->put('report/{id}', 'App\\Api\\V1\\Controllers\\ReportsController@updateReport')->middleware('role:admin_user|inspector_user');

        $api->get('refresh', [
            'middleware' => 'jwt.refresh',
            function () {
                return response()->json([
                    'message' => 'Token has been refreshed. Check headers!'
                ]);
            }
        ]);
    });
});
