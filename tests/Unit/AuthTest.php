<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Faker\Factory as Faker;
use App\Models\User;

class AuthTest extends TestCase {

    private $name, $email, $password;

    protected function setUp() : void {
        parent::setUp();
        $faker = Faker::create();
        $this->name = $faker->name;
        $this->email = $faker->unique()->safeEmail;
        $this->password = 'password';
        $user = new User([
            'name' => $this->name,
            'email' => $this->email,
            'password' => $this->password
        ]);
        $user->save();
    }

    public function testSingup(){
        $faker = Faker::create();
        $payload = [
            'name' => $faker->name,
            'email' => $faker->unique()->safeEmail,
            'password' => $this->password
        ];
        $response = $this->post('/api/auth/signup', $payload);
        $response->assertStatus(201);   // HTTP 201 Created
    }

    public function testLogin() {
        $credenciales = [
            'email' => $this->email,
            'password' => $this->password
        ];
        $response = $this->post('/api/auth/login', $credenciales);
        $response->assertStatus(200);
        $data = $response->decodeResponseJson();
        $this->assertArrayHasKey('token', $data);
    }

    public function testSendResetEmail(){
        $response = $this->post('/api/auth/recovery', ['email' => $this->email]);
        $response->assertStatus(200);
    }

    public function testResetPassword() {
        $token = $this->getToken();
        $payload = [
            'email' => $this->email,
            'token' => $token
        ];
        $response = $this->post('/api/auth/reset', $payload);
        $response->assertStatus(200);
    }

    public function testLogout(){
        $token = $this->getToken();
        $response = $this->post('/api/auth/logout', ['token' => $token]);
        $response->assertStatus(200);
    }

    public function testRefresh(){
        $token = $this->getToken();
        $response = $this->post('/api/auth/refresh', ['token' => $token]);
        $response->assertStatus(200);
    }

    public function testMe() {
        $token = $this->getToken();
        $response = $this->get('/api/auth/me', ['token' => $token]);
        $response->assertStatus(200);
        $data = $response->decodeResponseJson();
        $this->assertArrayHasKey('id', $data);
        $this->assertArrayHasKey('name', $data);
        $this->assertArrayHasKey('email', $data);
        $this->assertArrayHasKey('phone', $data);
        $this->assertArrayNotHasKey('password', $data);
        $this->assertArrayNotHasKey('remember_token', $data);
    }

    private function getToken() : String {
        $credenciales = [
            'email' => $this->email,
            'password' => $this->password
        ];
        $response = $this->post('/api/auth/login', $credenciales);
        $data = $response->decodeResponseJson();
        return $data['token'];
    }
}
