<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Faker\Factory as Faker;
use App\Models\Type;
use Tests\TestCase;

class TypeTest extends TestCase {

    protected function setUp() : void {
        parent::setUp();
        $faker = Faker::create();
        $this->name = $faker->name;
        $this->email = $faker->unique()->safeEmail;
        $this->password = 'password';
        $user = new User([
            'name' => $this->name,
            'email' => $this->email,
            'password' => $this->password
        ]);
        $user->save();
    }

    public function testCreateType(){
        $payload = [
            'token' => $this->getToken(),
            'type' => Str::random(14)
        ];
        $response = $this->post('/api/type', $payload);
        $response->assertStatus(201);   // HTTP 201 Created
    }

    public function testGetTypes(){
        $response = $this->get('/api/types');
        $response->assertStatus(200);
    }

    public function testUpdateType(){
        $type = new Type([
            'name' => Str::random(14)
        ]);
        $type->save();
        $payload = [
            'token' => $this->getToken(),
            'id' => $type->id,
            'type' => Str::random(14)
        ];
        $response = $this->put('/api/type', $payload);
        $response->assertStatus(201);   // HTTP 201 Created
    }

    public function testDeleteType(){
        $type = new Type([
            'name' => Str::random(14)
        ]);
        $type->save();
        $payload = [
            'token' => $this->getToken(),
            'id' => $type->id,
        ];
        $response = $this->delete('/api/type', $payload);
        $response->assertStatus(200);
    }

    private function getToken() : String {
        $credenciales = [
            'email' => $this->email,
            'password' => $this->password
        ];
        $response = $this->post('/api/auth/login', $credenciales);
        $data = $response->decodeResponseJson();
        return $data['token'];
    }
    
}
