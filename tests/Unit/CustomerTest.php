<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Faker\Factory as Faker;
use App\Models\Customer;
use Tests\TestCase;

class CustomerTest extends TestCase {

    protected function setUp() : void {
        parent::setUp();
        $faker = Faker::create();
        $this->name = $faker->name;
        $this->email = $faker->unique()->safeEmail;
        $this->password = 'password';
        $user = new User([
            'name' => $this->name,
            'email' => $this->email,
            'password' => $this->password
        ]);
        $user->save();
    }

    public function testCreateCustomer(){
        $faker = Faker::create();
        $payload = [
            'token' => $this->getToken(),
            'name' => $faker->name,
            'email' => $faker->unique()->safeEmail,
            'phone' => $faker->phoneNumber,
            'age' => $faker->randomDigitNotNull
        ];
        $response = $this->post('/api/customer', $payload);
        $response->assertStatus(201);   // HTTP 201 Created
    }

    public function testGetCustomers(){
        $response = $this->get('/api/customers');
        $response->assertStatus(200);
    }

    public function testUpdateCustomer(){
        $faker = Faker::create();
        $customer = new Municipio([
            'name' => $faker->name,
            'email' => $faker->unique()->safeEmail,
            'phone' => $faker->phoneNumber,
            'age' => $faker->randomDigitNotNull
        ]);
        $customer->save();
        $payload = [
            'token' => $this->getToken(),
            'name' => $faker->name,
            'email' => $faker->unique()->safeEmail,
            'phone' => $faker->phoneNumber,
            'age' => $faker->randomDigitNotNull
        ];
        $response = $this->put('/api/customer', $payload);
        $response->assertStatus(201);   // HTTP 201 Created
    }

    public function testDeleteCustomer(){
        $faker = Faker::create();
        $customer = new Municipio([
            'name' => $faker->name,
            'email' => $faker->unique()->safeEmail,
            'phone' => $faker->phoneNumber,
            'age' => $faker->randomDigitNotNull
        ]);
        $customer->save();
        $payload = [
            'token' => $this->getToken(),
            'id' => $customer->id,
        ];
        $response = $this->delete('/api/customer', $payload);
        $response->assertStatus(200);
    }

    private function getToken() : String {
        $credenciales = [
            'email' => $this->email,
            'password' => $this->password
        ];
        $response = $this->post('/api/auth/login', $credenciales);
        $data = $response->decodeResponseJson();
        return $data['token'];
    }
}
