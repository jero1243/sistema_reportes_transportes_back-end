<?php

return [
    // Configurations for the user
    'users' => [
        // Roles
        'admin_role' => 'admin_user',
        'default_role' => 'base_user',
        'visor_role'=> 'visor_user',
        'gestor_role'=>'gestor_user',
        'inspector_role'=>'inspector_user'
    ],
];
