<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBitacorasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bitacoras', function (Blueprint $table) {
            $table->increments('id');
            $table->string('actividad')->nullable(false);
            $table->unsignedInteger('user_id')->nullable(false);
            $table->unsignedInteger('previous_user_id');
            $table->unsignedInteger('reporte_id')->nullable(false);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('previous_user_id')->references('id')->on('users');
            $table->foreign('reporte_id')->references('id')->on('reportes');

            $table->index(['id', 'reporte_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bitacoras');
    }
}
